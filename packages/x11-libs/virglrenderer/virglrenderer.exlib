# Copyright 2016-2019 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

if ever is_scm ; then
    require gitlab [ prefix=https://gitlab.freedesktop.org user=virgl ]
else
    require gitlab [ prefix=https://gitlab.freedesktop.org user=virgl tag=${PNV} new_download_scheme=true ]
fi

require meson

SUMMARY="Virgl rendering library"
DESCRIPTION="
The virgil3d rendering library is used by QEMU to implement 3D GPU support for the virtio GPU.
"
HOMEPAGE+=" https://virgil3d.github.io"

LICENCES="MIT"
SLOT="0"
MYOPTIONS="X"

# Tests try to access /dev/dri/* resulting in access violations in sydbox
RESTRICT="test"

DEPENDENCIES="
    build:
        dev-lang/python:*
        virtual/pkg-config[>=0.9.0]
        x11-utils/util-macros[>=1.8]
    build+run:
        dev-libs/libepoxy[X?][>=1.5.4]
        x11-dri/libdrm[>=2.4.50]
        x11-dri/mesa[>=18.0.0] [[ note = [ gbm ] ]]
        X? ( x11-libs/libX11 )
    test:
        dev-libs/check[>=0.9.4]
"

! ever is_scm && MESON_SOURCE=${WORKBASE}/${PN}-${PNV}

MESON_SRC_CONFIGURE_PARAMS=(
    -Dfuzzer=false
    -Dgbm_allocation=true
    -Dvalgrind=false
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'X with_x11 yes no'
    'X with_glx yes no'
)

