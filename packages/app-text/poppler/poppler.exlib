# Copyright 2008-2012 Ingmar Vanhassel <ingmar@exherbo.org>
# Copyright 2008, 2010 Saleem Abdulrasool <compnerd@compnerd.org>
# Copyright 2013-2021 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require cmake utf8-locale

export_exlib_phases src_configure src_test

SUMMARY="A PDF rendering library, based on Xpdf"
DESCRIPTION="
Poppler is a PDF rendering library derived from xpdf. It has been enhanced to
utilize modern libraries such as freetype and cairo for better rendering. It
also provides basic command line utilities.
"
HOMEPAGE="https://${PN}.freedesktop.org"

if ever at_least 21.04.0 ; then
    TEST_REV="03a4b9eb854a06a83c465e82de601796c458bbe9"
    DOWNLOADS="${HOMEPAGE}/${PNV}.tar.xz
        https://gitlab.freedesktop.org/poppler/test/-/archive/${TEST_REV}/test-${TEST_REV}.tar.bz2"
else
    # NOTE: test data tarball:
    #   https://gitlab.freedesktop.org/poppler/test.git
    #   git archive --format=tar --prefix=test/ HEAD | xz > ${PN}-test-yyyymmdd.tar.xz
    DOWNLOADS="${HOMEPAGE}/${PNV}.tar.xz
        https://dev.exherbo.org/distfiles/${PN}/${PN}-test-20090513.tar.xz"
fi

LICENCES="|| ( GPL-2 GPL-3 )"
SLOT="0"
MYOPTIONS="
    cairo [[ description = [ Build the cairo graphics backend ] ]]
    curl [[ description = [ Use libcurl for HTTP support ] ]]
    glib [[
        description = [ Build the GLib wrapper ]
        requires = [ cairo ]
    ]]
    gobject-introspection [[ requires = [ glib ] ]]
    jpeg2000
    lcms
    qt5 [[ description = [ Build the Qt5 wrapper ] ]]
    tiff

    ( cairo qt5 ) [[ number-selected = at-least-one ]]
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

if ever at_least 21.04.0 ; then
    MYOPTIONS+="
        boost [[ description = [ Use boost's headers for better performance of the Splash backend ] ]]
    "
fi

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.9.0]
        gobject-introspection? ( gnome-desktop/gobject-introspection:1 )
    build+run:
        app-text/poppler-data[>=0.4.7]
        dev-libs/nspr
        dev-libs/nss[>=3.19]
        media-libs/fontconfig[>=2.0.0]
        media-libs/freetype:2[>=2.1.8]
        media-libs/libpng:=
        sys-libs/zlib
        cairo? ( x11-libs/cairo[>=1.10.0] )
        curl? ( net-misc/curl )
        glib? ( dev-libs/glib:2[>=2.41] )
        jpeg2000? ( media-libs/OpenJPEG:2 )
        lcms? ( media-libs/lcms2 )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        qt5? ( x11-libs/qtbase:5 )
        tiff? ( media-libs/tiff )
"

if ever at_least 21.04.0 ; then
    DEPENDENCIES+="
        build:
            dev-util/gperf
            boost? ( dev-libs/boost[>=1.58.0] )
            glib? ( dev-libs/glib:2[>=2.56] )
            qt5? ( x11-libs/qtbase:5[>=5.9] )
    "
fi

poppler_src_configure() {
    local cmakeargs=(
        -DBUILD_CPP_TESTS:BOOL=$(expecting_tests TRUE FALSE)
        -DENABLE_CPP:BOOL=TRUE
        -DENABLE_DCTDECODER:STRING=libjpeg
        -DENABLE_LIBOPENJPEG:STRING=$(option jpeg2000 openjpeg2 none )
        -DENABLE_SPLASH:BOOL=TRUE
        -DENABLE_UTILS:BOOL=TRUE
        -DENABLE_ZLIB:BOOL=TRUE
        -DENABLE_ZLIB_UNCOMPRESS:BOOL=FALSE
        -DWITH_JPEG:BOOL=TRUE
        -DWITH_NSS3:BOOL=TRUE
        -DWITH_PNG:BOOL=TRUE
        # GTK and it\'s tests seem to be unused as of poppler-0.22.1
        -DBUILD_GTK_TESTS:BOOL=FALSE
        -DWITH_GTK:BOOL=FALSE
        $(cmake_enable curl LIBCURL)
        $(cmake_with cairo Cairo)
        $(cmake_with glib GLIB)
        $(cmake_with tiff TIFF)
    )

    if option qt5 ; then
        cmakeargs+=(
            -DBUILD_QT5_TESTS:BOOL=$(expecting_tests TRUE FALSE)
        )
    fi

    if ever at_least 21.04.0 ; then
        cmakeargs+=(
            -DBUILD_QT6_TESTS:BOOL=FALSE
            -DBUILD_SHARED_LIBS:BOOL=TRUE
            -DENABLE_CMS:STRING=$(option lcms lcms2 none )
            -DENABLE_GTK_DOC:BOOL=FALSE
            -DENABLE_QT6:BOOL=FALSE
            -DENABLE_UNSTABLE_API_ABI_HEADERS:BOOL=TRUE
            -DRUN_GPERF_IF_PRESENT:BOOL=TRUE
            -DTESTDATADIR:PATH="${WORKBASE}"/test-${TEST_REV}
            $(cmake_enable glib GLIB)
            $(cmake_enable gobject-introspection GOBJECT_INTROSPECTION)
            $(cmake_enable qt5 QT5)
            $(cmake_disable_find Boost)
        )
    else
        cmakeargs+=(
            -DSHARE_INSTALL_DIR=/usr/share
            -DSPLASH_CMYK:BOOL=FALSE
            -DENABLE_CMS:STRING=$(option lcms lcms2 )
            -DENABLE_XPDF_HEADERS:BOOL=TRUE
            -DWITH_Qt4:BOOL=FALSE
            $(cmake_disable_find qt5 Qt5Core)
            $(cmake_disable_find qt5 Qt5Gui)
            $(cmake_disable_find qt5 Qt5Test)
            $(cmake_disable_find qt5 Qt5Widgets)
            $(cmake_disable_find qt5 Qt5Xml)
            $(cmake_with gobject-introspection GObjectIntrospection)
        )
    fi

    ecmake \
        "${cmakeargs[@]}"
}

poppler_src_test() {
    # qt4/tests/check_password.cpp fails with LC_ALL=C
    require_utf8_locale

    default
}

