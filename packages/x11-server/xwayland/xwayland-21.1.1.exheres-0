# Copyright 2021 Marc-Antoine Perennou <keruspe@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require xorg meson

SUMMARY="The X server for wayland"
DOWNLOADS="https://www.x.org/releases/individual/xserver/${PNV}.tar.xz"

LICENCES="X11"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    eglstream [[ description = [ xwayland eglstream support (proprietary Nvidia driver) ] ]]
"

# FIXME
RESTRICT="test"

DEPENDENCIES="
    build:
        x11-proto/xorgproto[>=2018.4]
    build+run:
        dev-libs/libepoxy
        sys-libs/wayland[>=1.3.0]
        sys-libs/wayland-protocols[>=1.18]
        x11-apps/xkbcomp
        x11-dri/libdrm[>=2.4.89]
        x11-dri/mesa[>=10.2.0]
        x11-libs/libX11[>=1.6]
        x11-libs/libXau
        x11-libs/libXdmcp
        x11-libs/libXext[>=1.0.99.4]
        x11-libs/libXfixes
        x11-libs/libXfont2[>=2.0.0]
        x11-libs/libXi[>=1.2.99.1]
        x11-libs/libxkbfile
        x11-libs/libXmu
        x11-libs/libXpm
        x11-libs/libXrender
        x11-libs/libXres
        x11-libs/libxshmfence[>=1.1]
        x11-libs/libXv[>=1.0.5]
        x11-libs/pixman:1[>=0.27.2]
        x11-libs/xtrans[>=1.3.5]
        x11-server/xorg-server[>=1.20.10-r1]
        eglstream? ( sys-libs/egl-wayland[>=1.0.2] )
"

# FIXME: enable secure-rpc
MESON_SRC_CONFIGURE_PARAMS=(
    -Ddefault_font_path="/usr/share/fonts/X11"
    -Ddri3=true
    -Dglamor=true
    -Dglx=true
    -Dsecure-rpc=false
    -Dxcsecurity=true
    -Dxv=true
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'eglstream xwayland_eglstream'
)

src_install() {
    meson_src_install
    edo rm -r "${IMAGE}"/usr/{share/man/man1/Xserver.1,$(exhost --target)/lib/xorg}
}

